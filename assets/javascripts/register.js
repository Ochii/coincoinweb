$(document).ready(function (e) {
    $('.passwordCheck').keyup(function () {
        setTimeout(function () {
            if ($("#password").val() != $("#passwordConfirm").val()) {
                $("#passwordConfirm").addClass('is-invalid').removeClass('is-valid');
            } else {
                $("#passwordConfirm").addClass('is-valid').removeClass('is-invalid');
            }
        }, 500);
    });
    $("#formRegister").submit(function () {
        event.preventDefault();
        $.ajax({
            url: '/index.php/Auth/register',
            type: 'POST',
            data: {
                mail: $("#mail").val(),
                firstname: $("#firstname").val(),
                lastname: $("#lastname").val(),
                password: $("#password").val()
            },
            dataType: 'json',
            success: function (donnees, status, xhr) {
                if (donnees.status == "Success") {
                    var notice = new PNotify({
                        title: donnees.status,
                        text: donnees.info,
                        type: 'success',
                        hide: true,
                        delay: 4000,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    setTimeout(function () {
                        location.reload();
                    }, 4000);
                    notice.get().click(function () {
                        notice.remove();
                    });
                    $('#registerButton').prop('disable', true);
                } else {
                    var notice = new PNotify({
                        title: donnees.status,
                        text: donnees.info,
                        type: 'error',
                        hide: true,
                        delay: 4000,
                        buttons: {
                            closer: true,
                            sticker: false
                        }
                    });
                }
            },
            error: function (xhr, status, error) {
                alert("param : " + xhr.responseText);
                alert("status : " + status);
                alert("error : " + error);
            }
        });
    });
});