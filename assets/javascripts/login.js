$(document).ready(function (e) {
    $(".resetPasswordLink").click(function () {
        $(".modal").modal('hide');
        $("#resetPasswordModal").modal('show');
    });
    $(".registerLink").click(function () {
        $(".modal").modal('hide');
        $("#registerModal").modal('show');
    });
    $(".connexionLink").click(function () {
        $(".modal").modal('hide');
        $("#connexionModal").modal('show');
    });
    $("#deconnexionButton").click(function () {
        event.preventDefault();
        $.ajax({
            url: '/index.php/Auth/disconnectMe',
            dataType: 'json',
            success: function (donnees, status, xhr) {
                if (donnees.status == "Success") {
                    var notice = new PNotify({
                        title: donnees.status,
                        text: donnees.info,
                        type: 'success',
                        hide: true,
                        delay: 4000,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    setTimeout(function () {
                        window.location.replace("/");
                    }, 2500);
                    notice.get().click(function () {
                        notice.remove();
                    });
                } else {
                    var notice = new PNotify({
                        title: donnees.status,
                        text: donnees.info,
                        type: 'error',
                        hide: true,
                        delay: 4000,
                        buttons: {
                            closer: true,
                            sticker: false
                        }
                    });
                }
            },
            error: function (xhr, status, error) {
                alert("param : " + xhr.responseText);
                alert("status : " + status);
                alert("error : " + error);
            }
        });
    });
    $("#connexionButton").click(function () {
        event.preventDefault();
        $.ajax({
            url: '/index.php/Auth/login',
            type: 'POST',
            data: {
                login: $("#login").val(),
                password: $("#password").val()
            },
            dataType: 'json',
            success: function (donnees, status, xhr) {
                if (donnees.status == "Success") {
                    var notice = new PNotify({
                        title: donnees.status,
                        text: donnees.info,
                        type: 'success',
                        hide: true,
                        delay: 4000,
                        buttons: {
                            closer: false,
                            sticker: false
                        }
                    });
                    setTimeout(function () {
                        $("#connexionModal").modal('hide');
                        setTimeout(function () {
                            window.location.replace("/");
                        }, 1500);
                    }, 1000);
                    notice.get().click(function () {
                        notice.remove();
                    });
                } else {
                    var notice = new PNotify({
                        title: donnees.status,
                        text: donnees.info,
                        type: 'error',
                        hide: true,
                        delay: 4000,
                        buttons: {
                            closer: true,
                            sticker: false
                        }
                    });
                }
            },
            error: function (xhr, status, error) {
                alert("param : " + xhr.responseText);
                alert("status : " + status);
                alert("error : " + error);
            }
        });
    });
    $('.passwordCheck').keyup(function () {
        setTimeout(function () {
            if ($("#passwordRegister").val() != $("#passwordConfirmRegister").val()) {
                $("#passwordConfirmRegister").addClass('is-invalid').removeClass('is-valid');
            } else {
                $("#passwordConfirmRegister").addClass('is-valid').removeClass('is-invalid');
            }
        }, 500);
    });
    $("#registerButton").click(function () {
        event.preventDefault();
        if ($("#passwordRegister").val() == $("#passwordConfirmRegister").val()) {
            $("#passwordConfirmRegister").addClass('is-valid').removeClass('is-invalid');
            $.ajax({
                url: '/index.php/Auth/register',
                type: 'POST',
                data: {
                    mail: $("#mail").val(),
                    firstname: $("#firstname").val(),
                    lastname: $("#lastname").val(),
                    password: $("#passwordRegister").val()
                },
                dataType: 'json',
                success: function (donnees, status, xhr) {
                    if (donnees.status == "Success") {
                        var notice = new PNotify({
                            title: donnees.status,
                            text: donnees.info,
                            type: 'success',
                            hide: true,
                            delay: 4000,
                            buttons: {
                                closer: false,
                                sticker: false
                            }
                        });
                        $("#login").val($("#mail").val());
                        setTimeout(function () {
                            $(".modal").modal('hide');
                            $("#connexionModal").modal('show');
                        }, 500);
                        notice.get().click(function () {
                            notice.remove();
                        });
                        $('#registerButton').prop('disable', true);
                    } else {
                        var notice = new PNotify({
                            title: donnees.status,
                            text: donnees.info,
                            type: 'error',
                            hide: true,
                            delay: 4000,
                            buttons: {
                                closer: true,
                                sticker: false
                            }
                        });
                    }
                },
                error: function (xhr, status, error) {
                    alert("param : " + xhr.responseText);
                    alert("status : " + status);
                    alert("error : " + error);
                }
            });
        } else {
            $("#passwordConfirmRegister").addClass('is-invalid').removeClass('is-valid');
        }
    });
});