<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trek extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/trek
     *    - or -
     *        http://example.com/index.php/trek/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {
        $js[0] = base_url() . "assets/javascripts/login.js";
        $data['js'] = $js;
        $this->load->view("templates/header");
        $this->load->view("templates/navbar");
        $this->load->view('welcome');
        $this->load->view('login');
        $this->load->view("templates/footer", $data);
    }

    public function trekdetails()
    {
        $js[0] = base_url() . "assets/javascripts/login.js";
        $data['js'] = $js;
        $this->load->view("templates/header");
        $this->load->view("templates/navbar");
        $this->load->view('trekdetails');
        $this->load->view('login');
        $this->load->view("templates/footer", $data);
    }

    public function treklist()
    {
        $js[0] = base_url() . "assets/javascripts/login.js";
        $data['js'] = $js;
        $this->load->view("templates/header");
        $this->load->view("templates/navbar");
        $this->load->view('treklist');
        $this->load->view('login');
        $this->load->view("templates/footer", $data);
    }
}