<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function login()
    {
        if (is_logged_in()) {
            $response['status'] = 'error';
            $response['info'] = 'Vous êtes déjà connecté';
        } else {
            $login = strtolower($this->input->post('login'));
            $password = $this->input->post('password');
            $this->db->select('*');
            $this->db->from('users');
            $this->db->where('mail', $login);
            $query = $this->db->get();
            if ($query->num_rows() == 1) {
                $row = $query->row();
                if (password_verify($password, $row->password)) {
                    $user = array(
                        'id' => $row->id,
                        'mail' => $row->mail,
                        'firstname' => $row->firstname,
                        'lastname' => $row->lastname,
                        'rank' => $row->rank,
                        'expire' => time() + 3600,
                        'isConnected' => 1,
                        'mailValide' => $row->mail_valid
                    );
                    $this->session->set_userdata($user);
                    if (!$row->mail_valid) {
                        $response['status'] = 'error';
                        $response['info'] = 'mail not confirmed';
                    } else {
                        $response['status'] = 'success';
                        $response['info'] = 'Vous êtes désormais connecté';
                    }
                } else {
                    $this->session->set_userdata('expire', time() - 100);
                    $this->session->set_userdata('isConnected', 0);
                    $response['status'] = 'error';
                    $response['info'] = 'Mail ou mot de passe incorrect';
                }
            } else {
                $this->session->set_userdata('expire', time() - 100);
                $this->session->set_userdata('isConnected', 0);
                $response['status'] = 'error';
                $response['info'] = 'Mail ou mot de passe incorrect';
            }
        }
        echo json_encode($response);
    }

    private function test()
    {
        $data = array(
            'mail' => 'guiduarte@hotmail.fr',
            'firstname' => 'jean'
        );
        $this->load->helper('mailer');
        //  mail_register($data);
    }

    public function mail_code_generator($regenerate = true)
    {
        $code = strtoupper(substr(md5(uniqid(mt_rand(), true)), 0, 6));
        if ($regenerate) {
            $query = $this->db->select('last_time_updated')->from('users')->where('id', $this->session->id)->get();
            if ($query->num_rows() == 1) {
                $row = $query->row();
                $last_time_updated = strtotime($row->last_time_updated);
                if(($last_time_updated + 10) < time()){
                    $data = array(
                        'mail' => $this->session->mail,
                        'lastname' => $this->session->lastname,
                        'firstname' => $this->session->firstname,
                        'mail_code' => $code
                    );
                    $this->db->set('mail_valid', 0);
                    $this->db->set('mail_code', $code);
                    $this->db->where('id', $this->session->id);
                    if ($this->db->update('users')) {
                        $this->load->helper('mailer');
                        mail_register($data);
                        $response['status'] = "success";
                        $response['info'] = "Le lien d'activation viens de vous être envoyé par mail";
                    } else {
                        $response['status'] = "error";
                        $response['info'] = "Impossible de renvoyer un lien d'activation, veuillez réessayer ultérieurement";
                    }
                }else{
                    $response['status'] = "warning";
                    $response['info'] = "Veuillez patienter quelques secondes avant de redemander un lien d'activation";
                }
            }
            echo json_encode($response);
        } else {
            return $code;
        }
    }

    public function mailValidator()
    {
        $code = strtoupper($this->input->get('code'));
        if (!empty($code)) {
            if ($this->db->where('mail_code', $code)->where('id', $this->session->id)->from('users')->count_all_results() == 1) {
                $this->db->set('mail_valid', 1);
                $this->db->where('id', $this->session->id);
                if ($this->db->update('users')) {
                    $response['status'] = 'Success';
                    $response['info'] = 'Compte validé !';
                    $this->session->set_userdata('mailValide', 1);
                    if (!$this->input->is_ajax_request()) {
                        redirect('/index.php/AutoClose');
                    }
                } else {
                    $response['status'] = 'error';
                    $response['info'] = 'Impossible de valider le compte, veuillez réessayer';
                }
            } else {
                $response['status'] = 'error';
                $response['info'] = 'Impossible de valider le compte, code faux';
            }
        } else {
            // Si on est dans le cas d'un auto check
            if ($this->input->is_ajax_request()) {
                $this->db->select('mail_valid, mail_code');
                $this->db->from('users');
                $this->db->where('id', $this->session->id);
                $query = $this->db->get();
                if ($query->num_rows() == 1) {
                    $row = $query->row();
                    if ($row->mail_valid == 1) {
                        $this->session->set_userdata('mailValide', 1);
                        $response['status'] = 'success';
                        $response['info'] = 'Compte validé !';
                        $response['code'] = $row->mail_code;
                    } else {
                        $response['status'] = 'Waiting';
                    }
                }
            } else {
                $response['status'] = 'error';
                $response['info'] = 'Impossible de valider le compte, veuillez saisir le code reçu';
            }
        }
        if (!empty($response))
            echo json_encode($response);
    }

    public function register()
    {
        if (!is_logged_in()) {
            $mail = strtolower($this->input->post('mail'));
            $password = password_hash($this->input->post('password'), PASSWORD_BCRYPT);
            $firstname = $this->input->post('firstname');
            $lastname = $this->input->post('lastname');
            $data = array(
                'mail' => $mail,
                'password' => $password,
                'lastname' => $lastname,
                'firstname' => $firstname,
                'ip' => $this->input->ip_address()
            );
            if ($this->db->where('mail', $mail)->from('users')->count_all_results() == 0) {
                $data['mail_code'] = $this->mail_code_generator(false);
                if ($this->db->insert('users', $data)) {
                    $this->load->helper('mailer');
                    mail_register($data);
                    $response['status'] = 'success';
                    $response['info'] = 'Compte crée avec succès';
                } else {
                    $response['status'] = 'error';
                    $response['info'] = 'Impossible de créer le compte, réessayez dans quelques instants';
                }
            } else {
                $response['status'] = 'error';
                $response['info'] = 'Cette adresse mail existe déjà';
            }
        } else {
            $response['status'] = 'error';
            $response['info'] = 'Vous êtes déjà connecté';
        }
        echo json_encode($response);
    }

    public function disconnectMe()
    {
        if (disconnect_user()) {
            $response['status'] = 'success';
            $response['info'] = 'Vous êtes déconnecté';
        } else {
            $response['status'] = 'error';
            $response['info'] = 'Impossible actuellement, merci de réessayer ultérieurement';
        }
        echo json_encode($response);
    }
}
