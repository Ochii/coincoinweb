<?php
function is_logged_in()
{
    // Get current CodeIgniter instance
    $CI =& get_instance();
    if ($CI->session->expire < time()) {
        return false;
    } elseif ($CI->session->isConnected != 1) {
        return false;
    } elseif ($CI->session->mailValide == 0) {
        return false;
    } else {
        return true;
    }
}

function only_if_connected()
{
    // Get current CodeIgniter instance
    $CI =& get_instance();
    if ($CI->session->expire < time()) {
        redirect('/');
    } elseif ($CI->session->isConnected != 1) {
        redirect('/');
    } else {
        return true;
    }
}

function disconnect_user()
{
    $CI =& get_instance();
    if ($CI->session->has_userdata('id')) {
        $CI->session->isConnected = 0;
        return session_destroy();
    } else {
        return true;
    }

}