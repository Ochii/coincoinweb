<?php defined('BASEPATH') OR exit('No direct script access allowed');

use Mailjet\Resources;

// old version based on SFTP
/*function mail_register($user)
{

    // Get current CodeIgniter instance
    $CI =& get_instance();
    $CI->load->library('email');
    $CI->email->to($user['mail']);
    $CI->email->from('coincoin@24server.fr', 'Coincoin');
    $CI->email->subject('Création de compte');
    $CI->email->message("Bienvenue sur Coincoin, l'outil de partage de parcours et de photos en ligne ! <br> " .
        ucfirst($user['firstname']) . " votre compte est dès à présent créer, il ne vous reste plus qu'a valider votre email en cliquant sur le lien ou saisissant ce code: <a href='https://coincoin.24server.fr/index.php/Auth/mailValidator?code=" .
        $user['mail_code'] . "'>" . $user['mail_code'] .
        "</a> <br> Vous pouvez accèder à la plateforme : <a href='https://coincoin.24server.fr'>https://coincoin.24server.fr</a>");
    $CI->email->send();

}
*/
function mail_register(array $user)
{
    $apikey = 'eee8216b3d2bbea79c1a01db3b2271ad';
    $apisecret = 'f3005a2b3f59439c1dd8801f4010c77a';
    $mj = new Mailjet\Client($apikey, $apisecret, true, ['version' => 'v3.1']);
    $user['mail_link'] = 'https://coincoin.24server.fr/index.php/Auth/mailValidator?code=' . $user['mail_code'];
    $variables = array(
        'firstname' => $user['firstname'],
        'confirmation_link' => $user['mail_link'],
        'mail_code' => $user['mail_code'],
    );
    $body = [
        'Messages' => [
            [
                'From' => [
                    'Email' => "coincoin@24server.fr",
                    'Name' => "Coincoin"
                ],
                'To' => [
                    [
                        'Email' => $user['mail'],
                        'Name' => $user['firstname']
                    ]
                ],
                'TemplateID' => 1173471,
                'TemplateLanguage' => true,
                'Subject' => "Création de compte",
                'Variables' => $variables
            ]
        ]
    ];
    $response = $mj->post(Resources::$Email, ['body' => $body]);
    //$response->success() && var_dump($response->getData());
    return $response->success();
}
