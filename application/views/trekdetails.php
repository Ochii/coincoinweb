<div class="container">
    <div class="row">
        <div class="col-6" id="imgmapdetail">
            <img src="/assets/images/italie-Carte-du-lac-de-come-1.jpg"/>
        </div>
        <div class="col-6" id="test">
            <div class="row align-items-start">
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Ut non sequi laboriosam laudantium, animi totam est sunt voluptatem, aperiam quibusdam optio, maxime ab nihil rerum sint ipsa doloremque tenetur repellat.
            </div>
            <div class="row" id="laRow">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">Pays : <span class="badge badge-primary badge-pill">France</span></li>
                    <li class="list-group-item">Région : <span class="badge badge-primary badge-pill">Alpes</span></li>
                    <li class="list-group-item">Difficulté : <span></span></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        Photos des lieux à voir.
    </div>
    <div class="row">
        <?php  $image = $this->session->firstname ?>
        <div class="col-4">
            <img src="<?php $image ?>"/>
        </div>
    </div>
</div>