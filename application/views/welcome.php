<div class="container-lg" id="toptreak">
    <h2>Participez à l'aventure grâce à l'application</h2> 
    <button class="btn btn-success btn-lg" type="submit" id="button-download">Télécharger CoincoinApp </button>
</div>
<div class="container-lg" id="toptreak">
    <div class="row" id="toprow">
        <div class="col-4" id="imgrow">
            <img src="assets/images/chemin-montagne.jpg"/>
        </div>
        <div class="col-8" id="textrow">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ornare est et dictum tincidunt. Curabitur placerat nulla eu lectus rutrum placerat.
            Aliquam accumsan at quam quis vestibulum. Cras vulputate consequat risus eget ultricies. Phasellus dolor turpis, tristique nec lacus at, imperdiet tincidunt nunc.
            Nunc faucibus massa lacus, quis molestie libero egestas id. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
            Donec non fringilla diam. Curabitur aliquam dolor sem, eu vehicula sem egestas eu. Donec ornare, sem vitae blandit blandit, libero tellus iaculis quam,
            pretium suscipit nibh felis ut mi. Proin at aliquet diam, quis fermentum dui. Ut volutpat mauris in quam dictum egestas. Vivamus vestibulum eleifend neque,
            at rhoncus elit aliquet a. Fusce sodales, metus pharetra interdum sollicitudin,
            lorem mi molestie tellus, in pellentesque arcu justo nec mi. Nullam tempus risus vitae accumsan facilisis. Maecenas sodales metus in lorem mollis ultricies.
            <br>
            <a class="btn btn-primary btn-lg" href="/index.php/trek/trekdetails" role="button" id="">Voir détails</a>
            <div id="divAddList">
                Ajouter à ma liste <button class="btn btn-success btn-lg" type="submit" id="buttonAddList">+ </button>
            </div>
        </div>
    </div>
    <div class="row" id="toprow">
        <div class="col-8" id="textrow">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ornare est et dictum tincidunt. Curabitur placerat nulla eu lectus rutrum placerat.
            Aliquam accumsan at quam quis vestibulum. Cras vulputate consequat risus eget ultricies. Phasellus dolor turpis, tristique nec lacus at, imperdiet tincidunt nunc.
            Nunc faucibus massa lacus, quis molestie libero egestas id. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
            Donec non fringilla diam. Curabitur aliquam dolor sem, eu vehicula sem egestas eu. Donec ornare, sem vitae blandit blandit, libero tellus iaculis quam,
            pretium suscipit nibh felis ut mi. Proin at aliquet diam, quis fermentum dui. Ut volutpat mauris in quam dictum egestas. Vivamus vestibulum eleifend neque,
            at rhoncus elit aliquet a. Fusce sodales, metus pharetra interdum sollicitudin,
            lorem mi molestie tellus, in pellentesque arcu justo nec mi. Nullam tempus risus vitae accumsan facilisis. Maecenas sodales metus in lorem mollis ultricies.
            <br>
            <a class="btn btn-primary btn-lg" href="/index.php/trek/trekdetails" role="button" id="">Voir détails</a>
            <div id="divAddList">
                Ajouter à ma liste <button class="btn btn-success btn-lg" type="submit" id="buttonAddList">+ </button>
            </div>

        </div>
        <div class="col-4" id="imgrow">
            <img src="assets/images/chemin-montagne2.jpg"/>
        </div>
    </div>
    <div class="row" id="toprow">
        <div class="col-4" id="imgrow">
            <img src="assets/images/chemin-montagne3.jpg"/>
        </div>
        <div class="col-8" id="textrow">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ornare est et dictum tincidunt. Curabitur placerat nulla eu lectus rutrum placerat.
            Aliquam accumsan at quam quis vestibulum. Cras vulputate consequat risus eget ultricies. Phasellus dolor turpis, tristique nec lacus at, imperdiet tincidunt nunc.
            Nunc faucibus massa lacus, quis molestie libero egestas id. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
            Donec non fringilla diam. Curabitur aliquam dolor sem, eu vehicula sem egestas eu. Donec ornare, sem vitae blandit blandit, libero tellus iaculis quam,
            pretium suscipit nibh felis ut mi. Proin at aliquet diam, quis fermentum dui. Ut volutpat mauris in quam dictum egestas. Vivamus vestibulum eleifend neque,
            at rhoncus elit aliquet a. Fusce sodales, metus pharetra interdum sollicitudin,
            lorem mi molestie tellus, in pellentesque arcu justo nec mi. Nullam tempus risus vitae accumsan facilisis. Maecenas sodales metus in lorem mollis ultricies.
            <br>
            <a class="btn btn-primary btn-lg" href="/index.php/trek/trekdetails" role="button" id="">Voir détails</a>
            <div id="divAddList">
                Ajouter à ma liste <button class="btn btn-success btn-lg" type="submit" id="buttonAddList">+ </button>
            </div>
        </div>
    </div>
</div>


