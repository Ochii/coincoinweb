<link rel="stylesheet" href="<?php echo base_url() ?>assets/stylesheets/login.css"/>
<body>
<!-- Modal Connexion -->
<div class="modal fade" id="connexionModal" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="connexionModal"
     aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Connexion</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="input-group mb-3 input-group-lg">
                        <div class="input-group-prepend">
                            <span class="input-group-text">@</span>
                        </div>
                        <input type="email" id="login" class="form-control" name="login"
                               style="padding-bottom: 5px;" placeholder="Adresse mail"
                               required>
                    </div>
                    <div class="input-group mb-3 input-group-lg">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-key"></i></span>
                        </div>
                        <input type="password" id="password" class="form-control" style="padding-bottom: 5px;"
                               name="login"
                               placeholder="Mot de passe"
                               required>
                    </div>
                    <div class="row">
                        <a class="registerLink" href="#">S'inscrire ?</a>
                    </div>
                    <div class="row">
                        <a class="resetPasswordLink" href="#">Mot de passe perdu ?</a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Fermer</button>
                <button type="button" id="connexionButton" class="btn btn-primary btn-lg">Connexion</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal Register -->
<div class="modal fade" id="registerModal" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="registerModal"
     aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Inscription</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="input-group mb-3 input-group-lg">
                        <input type="text" id="firstname" class="form-control" name="login"
                               style="padding-bottom: 5px;" placeholder="Prénom"
                               required>
                    </div>
                    <div class="input-group mb-3 input-group-lg">
                        <input type="text" id="lastname" class="form-control" name="login"
                               style="padding-bottom: 5px;" placeholder="Nom"
                               required>
                    </div>
                    <div class="input-group mb-3 input-group-lg">
                        <div class="input-group-prepend">
                            <span class="input-group-text">@</span>
                        </div>
                        <input type="email" id="mail" class="form-control" name="login"
                               style="padding-bottom: 5px;" placeholder="Adresse mail"
                               required>
                    </div>
                    <div class="input-group mb-3 input-group-lg">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-key"></i></span>
                        </div>
                        <input type="password" id="passwordRegister" class="form-control passwordCheck" name="login"
                               style="padding-bottom: 5px;"
                               placeholder="Mot de passe" required>
                    </div>
                    <div class="input-group mb-3 input-group-lg">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-key"></i></span>
                        </div>
                        <input type="password" id="passwordConfirmRegister" class="form-control passwordCheck"
                               name="login"
                               style="padding-bottom: 5px;"
                               placeholder="Confirmer" required>
                    </div>
                    <div class="row">
                        <a class="connexionLink" href="#">Se connecter ?</a>
                    </div>
                    <div class="row">
                        <a class="resetPasswordLink" href="#">Mot de passe perdu ?</a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Fermer</button>
                <button type="button" id="registerButton" class="btn btn-primary btn-lg">S'inscrire</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal reset password -->
<div class="modal fade" id="resetPasswordModal" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="resetPasswordModal"
     aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Mot de passe perdu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="input-group mb-3 input-group-lg">
                        <div class="input-group-prepend">
                            <span class="input-group-text">@</span>
                        </div>
                        <input type="email" id="mailReset" class="form-control" name="login"
                               style="padding-bottom: 5px;" placeholder="Adresse mail"
                               required>
                    </div>
                    <div class="row">
                        <a class="connexionLink" href="#">Se connecter ?</a>
                    </div>
                    <div class="row">
                        <a class="registerLink" href="#">S'inscrire ?</a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Fermer</button>
                <button type="button" id="resetButton" class="btn btn-primary btn-lg">Réinitialiser</button>
            </div>
        </div>
    </div>
</div><!-- Modal Confirmation mail -->
<div class="modal fade" id="mailConfirmModal" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="mailConfirmModal"
     aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Confirmation du mail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active" aria-current="page">Veuillez cliquer sur le lien reçu par mail ou saisir le code afin de valider votre compte.</li>
                        </ol>
                    </nav>
                    <div class="input-group mb-3 input-group-lg">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-key"></i></span>
                        </div>
                        <input type="text" id="mailCode" class="form-control passwordCheck" name="mailCode"
                               style="padding-bottom: 5px;"
                               placeholder="Code" required>
                    </div>
                    <div class="d-flex justify-content-center">
                        <div id="mailLoader" class="spinner-border text-primary" style="width:30px;height: 30px;" role="status">
                            <span class="sr-only"></span>
                        </div>
                    </div>
                    <div class="row">
                        <a id="resendMailButton" href="#">Renvoyer le lien par email ?</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</body>
</html>