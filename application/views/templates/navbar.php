<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="/"><img src="/assets/images/!happy-face.png"></img></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
            aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/index.php/trek/treklist">Les Treks</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Ma liste</a>
            </li>
        </ul>
        <?php if (is_logged_in()) {
            echo '<div class="dropdown">
                    <a class="btn btn-secondary dropdown-toggle btn-lg" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    ' . ucfirst($this->session->firstname) . ' </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a id="deconnexionButton" class="dropdown-item" href="#">Déconnexion</a>
                    </div>
                </div>';
        } else {
            echo '<a href="#" data-toggle="modal" data-target="#connexionModal"><img style="width: 32px;height:32px;" src="/assets/images/user.png"/></a>';
        }
        ?>
    </div>
</nav>